from dolfin import *
import mshr as mr
import numpy as np
import matplotlib.pyplot as plt

"""
This demo program solves the Marguerre-von Karman equations for a cylindrical
vault of square planform clamped on one of its edge, simply supported 
on other two and subjected to a vertical point force on the fourth, free, edge.

It is provided as supplementary material to the article:

M. Brunetti, A. Favata, S. Vidoli. A low-order mixed variational principle
for the generalized Marguerre-von Karman equations. Submitted.

It has been implemented in Python using the FEniCS framework:

https://fenicsproject.org/

Dolfin version: 2018.1.0
"""

# Form compiler options
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True

# Geometry
Lx = 1.0
Ly = 1.0
t0 = Constant(0.01)
phi = np.pi/10.0
R, P_max = (0.5*Ly)/(np.sin(phi)), 1.0

# Elastic costants and bending stiffness
E = 1.0/t0**3
nu = Constant(0.3)
D = (E*t0**3)/(12.0*(1.0 - nu**2))

# Load history
P_values = np.linspace(0.0, P_max, 50)

# Initial shape
initial_shape = "sqrt(R*R - x[1]*x[1]) - 0.5*sqrt(4.0*R*R - Ly*Ly)"
w0 = Expression(initial_shape, R=R, Ly=Ly, degree=3)
q0 = Expression((initial_shape, "0.0"), R=R, Ly=Ly, degree=3)

# Mesh
domain = mr.Rectangle(Point(0.0, -Ly/2.0), Point(Lx, Ly/2.0))
mesh = mr.generate_mesh(domain, 21)

# Mesh size and facet normals
h = CellDiameter(mesh)
h_min = mesh.hmin()
h_avg = (h('+') + h('-'))/2.0
n = FacetNormal(mesh)

# Penalty parameters
eta_w, eta_ph = Constant(1E2), Constant(1E2)

# Element
P2 = FiniteElement("P", mesh.ufl_cell(), degree = 2)
P3 = FiniteElement("P", mesh.ufl_cell(), degree = 3)
element = MixedElement([P3] + [P3])

# Discrete function space
Q = FunctionSpace(mesh, element)
q, q_, qt = TrialFunction(Q), Function(Q), TestFunction(Q)

# Primary variables
w_, ph_ = split(q_)
wt, pht = split(qt)

# Domain and boundary
domains = MeshFunction("size_t", mesh, mesh.topology().dim())
boundaries = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)

# Boundary measure
dss = ds(subdomain_data=boundaries)

# Boundaries
class Up(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs(x[1] - Ly/2.0) <= DOLFIN_EPS

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs(x[1] + Ly/2.0) <= DOLFIN_EPS

class Left(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs(x[0]) <= DOLFIN_EPS

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and abs(x[0] - Lx) <= DOLFIN_EPS

# Boundary markers
boundaries.set_all(10)
left_boundary, bottom_boundary, right_boundary, up_boundary = Left(), Bottom(), Right(), Up()
left_boundary.mark(boundaries, 1)
bottom_boundary.mark(boundaries, 2)
right_boundary.mark(boundaries, 3)
up_boundary.mark(boundaries, 4)

# Boundary conditions
bc_w_up = DirichletBC(Q.sub(0), project(w0, Q.sub(0).collapse()), up_boundary)
bc_w_bot = DirichletBC(Q.sub(0), project(w0, Q.sub(0).collapse()), bottom_boundary)
bc_w_left = DirichletBC(Q.sub(0), project(w0, Q.sub(0).collapse()), left_boundary)
bc_ph = DirichletBC(Q.sub(1), Constant(0.0), "on_boundary")
bcs = [bc_w_up, bc_w_bot, bc_w_left, bc_ph]

# M, Phi and W tensors
w0i = project(w0, Q.sub(0).collapse())
M = lambda z: D*(grad(grad(z)) + nu*cofac(grad(grad(z))))
Ph = lambda ph: (1.0/(E*t0))*(grad(grad(ph)) - nu*cofac(grad(grad(ph))))
W = lambda z: 0.5*(inner(grad(z), grad(z))*Identity(2) - outer(grad(z), grad(z)))

# Bending energy, membrane energy, coupling compatibility term
Bh = 0.5*inner(M(w_ - w0i), grad(grad(w_ - w0i)))*dx
Mh = 0.5*inner(Ph(ph_), grad(grad(ph_)))*dx
Ch = 0.5*inner(cofac(grad(grad(ph_))), outer(grad(w_), grad(w_)) - outer(grad(w0i), grad(w0i)))*dx

# CDG linear forms
jh_w = lambda zt, z: - jump(grad(zt), n)*avg(inner(M(z), outer(n, n)))*dS - jump(grad(z), n)*avg(inner(M(zt), outer(n, n)))*dS
jh_ph = lambda pht, ph: - jump(grad(pht), n)*avg(inner(Ph(ph), outer(n, n)))*dS - jump(grad(ph), n)*avg(inner(Ph(pht), outer(n, n)))*dS

# CDG linear forms on boundary
jh_bw = lambda zt, z: - inner(grad(zt), n)*inner(M(z), outer(n, n))*dss(1) - inner(grad(z), n)*inner(M(zt), outer(n, n))*dss(1)
sh_bw = lambda zt, z: Constant(1.E3)/h*inner(grad(zt), n)*inner(grad(z), n)*dss(1)

jh_bph = lambda pht, ph: - inner(grad(pht), n)*inner(Ph(ph), outer(n, n))*dss - inner(grad(ph), n)*inner(Ph(pht), outer(n, n))*dss
sh_bph = lambda pht, ph: Constant(1.E3)/h*inner(grad(pht), n)*inner(grad(ph), n)*dss

# New coupling functional jump-form
jh_w_ph = lambda z, pht: avg(inner(W(z), outer(n, n)))*jump(grad(pht), n)*dS

# Stabilisation terms
sh = lambda ft, f, eta: eta('+')/h_avg*jump(grad(ft), n)*jump(grad(f), n)*dS

# Forms
a_w = derivative(Bh, w_, wt) + jh_w(wt, w_ - w0i) + sh(wt, w_ - w0i, eta_w) + derivative(Ch, w_, wt)
a_ph = derivative(Mh, ph_, pht) + jh_ph(ph_, pht) + sh(pht, ph_, eta_ph) + jh_w_ph(w_, pht) - jh_w_ph(w0i, pht) - derivative(Ch, ph_, pht)

# Weak formulation
dPi = a_w - a_ph - jh_bph(pht, ph_) - sh_bph(pht, ph_) + jh_bw(wt, w_ - w0i) + sh_bw(wt, w_ - w0i)
J = derivative(dPi, q_, q)

# Special class for point force
class NonlinearProblemPointSource(NonlinearProblem):

    def __init__(self, L, a, bcs):
        NonlinearProblem.__init__(self)
        self.L = L
        self.a = a
        self.bcs = bcs
        self.P = 0.0

    def F(self, b, x):
        assemble(self.L, tensor=b)
        point_source = PointSource(self.bcs[0].function_space(), Point(Lx, 0.0), self.P)
        point_source.apply(b)
        for bc in self.bcs:
            bc.apply(b, x)

    def J(self, A, x):
        assemble(self.a, tensor=A)
        for bc in self.bcs:
            bc.apply(A, x)

# Nonlinear variational problem
problem = NonlinearProblemPointSource(dPi, J, bcs)

# Nonlinear solver
solver = NewtonSolver()
solver.parameters["linear_solver"] = "lu"
solver.parameters["convergence_criterion"] = "incremental"
solver.parameters["relative_tolerance"] = 1e-6
solver.parameters["absolute_tolerance"] = 1e-9

# Initial conditions
q_.assign(q0)

# Output files
out_dir = "output/"
file_w, file_ph = File(out_dir + "w.pvd"), File(out_dir + "ph.pvd")
file_w0 = File(out_dir + "w0.pvd")
file_w0 << w0i
ls_wL2, ls_phL2, ls_P = [], [], []

# Solution
for count, i in enumerate(P_values):

	print('------------------------------------------------')
	print('Now solving load increment ' + repr(count) + ' of ' + repr(len(P_values)))

	# Solve
	problem.P = i
	solver.solve(problem, q_.vector())
	w_h, ph_h = q_.split(deepcopy=True)

	# Store primal variables
	file_w << (w_h, i)
	file_ph << (ph_h, i)

	# Compute L2 norm
	deltaw_h = project(w_h - w0i, FunctionSpace(mesh, 'P', 3))

	# Save data for plot
	ls_P.append(i)
	ls_wL2.append(norm(deltaw_h, 'l2'))
	ls_phL2.append(norm(ph_h, 'l2'))
	

# w and P normalisation
normalised_wL2 = [j/float(t0) for j in ls_wL2]
normalised_phL2 = [j/float(D) for j in ls_phL2]
normalised_P = [j/float(D) for j in ls_P]

# Plot
plt.figure(1)
plt.plot(normalised_P, normalised_wL2, '-o', linewidth=2.5, color='green')
plt.xlabel(r"$P L \, / \, D$")
plt.ylabel(r"$|| w ||_2 \, / \, t_0$")
plt.grid()
plt.savefig(out_dir + "wL2.png")

plt.figure(2)
plt.plot(normalised_P, normalised_phL2, '-o', linewidth=2.5, color='brown')
plt.xlabel(r"$P L \, / \, D$")
plt.ylabel(r"$|| \varphi ||_2 \, / \, D$")
plt.grid()
plt.savefig(out_dir + "phL2.png")